let filter f s =
  let buf = Buffer.create (String.length s) in
  String.iter (fun c -> if f c then Buffer.add_char buf c) s ;
  Buffer.contents buf

