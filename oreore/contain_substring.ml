(* https://stackoverflow.com/questions/11193783/ocaml-strings-and-substrings*)
let contains word from =
  try
    let len = String.length word in
    for i = 0 to String.length from - len do
      if String.sub from i len = word then raise Exit
    done ;
    false
  with Exit -> true


(*自分で覚えて実装してみた*)
let f word target =
  let len = String.length word in
  try
    for i = 0 to String.length target - len do
      if String.sub target i len = word then raise Exit
    done ;
    false
  with Exit -> true

