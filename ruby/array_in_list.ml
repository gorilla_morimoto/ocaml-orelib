let drop n l =
  if n < 0 then raise (Invalid_argument "n must be greater then 0") ;
  if n > List.length l then raise (Invalid_argument "n is out of bounds") ;
  let rec f i l' = if i = 0 then l' else f (i - 1) (List.tl l') in
  f n l


let take n l =
  let rec f i tmp l' =
    if i = 0 then tmp
    else match l' with [] -> tmp | hd :: tl -> f (i - 1) (hd :: tmp) tl
  in
  f n [] l


let () =
  let l = [1; 2; 3; 4] in
  take 5 l |> List.iter (Printf.printf "%d\n")

